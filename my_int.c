#include "my_int.h"


void (*fp)(void)=NULL;

void init_handler(void)
{
  /* Assign function ctrlC_handler to handle SIGINT */
  signal(SIGINT, ctrlC_handler);

}

void set_user_handler(void (*user_handler)())
{
  fp = user_handler;
}


void ctrlC_handler(int signal)
{
	printf("This is Ctrl-C.\n");
  if(fp!=NULL){
    (*fp)();
  }
}