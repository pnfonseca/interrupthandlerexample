#include "my_int.h"

#include <stdlib.h>


void my_func1(void);
void my_func2(void);


int main(int argc, char *argv[])
{
  char c;

  init_handler();

  puts("Type your choice and press ENTER.");
  puts("1 - Selects first user function to be called on Ctrl-C.");
  puts("2 - Selects second user function to be called on Ctrl-C.");
  puts("x - Exit\n");
  puts("You can now press Ctrl-C...\n");

  /* Main cycle */
  while(1)
    {
      c = fgetc(stdin);
      
      switch(c){
        case '1':
          puts("Select my_func1");
          set_user_handler(my_func1);
          break;
        
        case '2':
          puts("Select my_func2");
          set_user_handler(my_func2);
          break;

        case 'x':
          exit(EXIT_SUCCESS);
      }

    }
}


void my_func1(void)
{
  puts("This is my_func1");
}

void my_func2(void)
{
  static int counter = 0;

  counter++;
  if(counter%2){
    printf("%s\n","Odd");
  }
  else{
    printf("%s\n","Even");  
  }
  puts("This is my_func2");

}

