#include <stdio.h>
#include <signal.h>

extern void (*fp)(void);

/* 
 * Interrupt handler for SIGINT (Ctrl-C pressed)
 */
void ctrlC_handler(int signal);

/*
 * Initialization of interrupt handler. 
 *
 * Register ctrlC_handler to be executed on SIGINT, i.e., 
 * when Ctrl-C is pressed.
 */
void init_handler(void);

/*
 * Assigns a user defined function to be the function
 * called by the interrupt handler.  
 */
void set_user_handler(void (*user_handler)());

