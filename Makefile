.PHONY = run

prog : main.o my_int.o
	gcc -o prog $^

run : prog
	./prog

clean : 
	rm -f *.o
	rm -f prog